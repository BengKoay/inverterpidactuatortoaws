const ModbusRTU = require('modbus-serial');
const cron = require('node-cron');
const moment = require('moment');

let client = new ModbusRTU();

client.connectRTUBuffered(
	'/dev/ttyAMA0',
	{ baudRate: 9600 },
	//   { baudRate: 38400, stopBits: 2 }, //  baudRate = 38400, to be changed to 9600 (standard), 1-start, 8-data, N-parity, 2-stop
	function (error, success) {
		if (error) {
			console.log('Serial Port initialization unsuccessful');
		} else {
			console.log('Serial port initialization successful');
		}
	}
);

client.setTimeout(500);

// client.setID(1); // device address
// var a = 0;
// var success = 0;
// var loss = 0;

const deviceId = 'e8219dfd-e549-498d-9258-7d99fb2b4870';
const deviceList = [
	{
		ref: 1,
		address: 1000,
	},
	{
		ref: 2,
		address: 1000,
	},
	{
		ref: 3,
		address: 8502,
	},
	{
		ref: 4,
		address: 8502,
	},
];

const result = [];
const main = async (time) => {
	cron.schedule(`${time} * * * * *`, async () => {
		//   console.log(deviceList);
		for (const eachDevice of deviceList) {
			console.log(eachDevice);
			console.log(eachDevice.ref);
			console.log(eachDevice.address);
			if (eachDevice.ref === 1 || eachDevice.ref === 2) {
				// ref 1, 2 is PID

				await client.setID(eachDevice.ref);

				await new Promise((resolve) => setTimeout(resolve, 10));
				energyParameters = await client.readInputRegisters(1000, 4);
				await new Promise((resolve) => setTimeout(resolve, 10));
				const pvValue = energyParameters.data[0];
				await new Promise((resolve) => setTimeout(resolve, 10));
				const svValue = energyParameters.data[3];
				// let sV = Math.random(10);
				let output = {
					ref: eachDevice.ref,
					sV: svValue,
					pV: pvValue,
				};
				result.push(output);
			}
			if (eachDevice.ref === 3 || eachDevice.ref === 4) {
				// ref 3, 4 is Inverter

				await client.setID(eachDevice.ref);
				await new Promise((resolve) => setTimeout(resolve, 10));
				energyParameters = await client.readHoldingRegisters(8502, 1);
				await new Promise((resolve) => setTimeout(resolve, 10));
				const frequency = energyParameters.buffer.readInt16BE(0) / 10;
				// let frequency = Math.random(10);
				let output = {
					ref: eachDevice.ref,
					refFrequency: frequency,
				};
				result.push(output);
			}
		}
		for (const eachObject of result) {
			eachObject.deviceId = deviceId;
			eachObject.timestamp = moment().unix();
		}
		console.log(result);
	});
};

// setInterval(() => {
main(50);
// }, 1000);
