const awsIot = require('aws-iot-device-sdk');
// const fs = require('fs-extra');
const ModbusRTU = require('modbus-serial');
const cron = require('node-cron');
const moment = require('moment');
const piConfig = require('./piConfig.js').piConfig;
// const appProperties = require('../helper/config/properties');
const awsIoTHost = 'a3j1bzslspz95c-ats.iot.ap-southeast-1.amazonaws.com';

const clientId = `${piConfig.lotId.replace(
	/-/g,
	''
)}_${piConfig.deviceId.replace(/-/g, '')}`;
// const dataPool = `${process.cwd()}/localdata/dataPool/processedData`;
// const publishingPath = `${dataPool}/publishing`;
// const publishedPath = `${dataPool}/published`;
// const readingTopic = `publish/lot_reading`;
const topic = 'iot/gabi_reading';
const publishingTopic = topic;
const subscribeTopic = topic;

const pubOpts = { qos: 1 };

const device = awsIot.device({
	clientId: clientId,
	// host: appProperties.awsIoTHost,
	host: awsIoTHost,
	offlineQueueing: false,
	clientCert: Buffer.from(piConfig.awsIoTCredentials.clientCert),
	privateKey: Buffer.from(piConfig.awsIoTCredentials.privateKey),
	caCert: Buffer.from(
		`-----BEGIN CERTIFICATE-----\nMIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF\nADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6\nb24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL\nMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv\nb3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj\nca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM\n9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw\nIFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6\nVOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L\n93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm\njgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC\nAYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA\nA4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI\nU5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs\nN+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv\no/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU\n5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy\nrqXRfboQnoZsG4q5WTP468SQvvG5\n-----END CERTIFICATE-----\n`
	),
});

let client = new ModbusRTU();

client.connectRTUBuffered(
	'/dev/ttyAMA0',
	{ baudRate: 38400, stopBits: 2 },
	//   { baudRate: 38400, stopBits: 2 }, //  baudRate = 38400, to be changed to 9600 (standard), 1-start, 8-data, N-parity, 2-stop
	function (error, success) {
		if (error) {
			console.log('Serial Port initialization unsuccessful');
		} else {
			console.log('Serial port initialization successful');
		}
	}
);
client.setTimeout(500);

const result = [];
const dataToPush = { data: [] };
const deviceId = '8e9da056-9297-4293-b24c-853c7a55ea79';
const deviceList = [
	{
		ref: 1,
		address: 0,
	},
	{
		ref: 2,
		address: 0,
	},
];
const main = async () => {
	device.on('connect', function () {
		device.subscribe(subscribeTopic);
		console.log('awsIoT connected');
	});

	// device.on('message', function (topic, payload) {
	// 	try {
	// 		if (!!payload) {
	// 			let data = JSON.parse(payload.toString());
	// 			console.log(data);
	// 		}
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// });

	device.on('close', function () {
		console.log('awsIoT disconnected');
		isDeviceConnected = false;
		reportedOnline = false;
	});

	device.on('error', function () {
		console.error('awsIoT error', arguments);
	});

	device.on('reconnect', function () {
		console.log('awsIoT reconnect...');
		isDeviceConnected = true;
	});

	device.on('timeout', function () {
		console.log('awsIoT timeout', arguments);
		isDeviceConnected = false;
		reportedOnline = false;
	});

	read(50);
};

const published = async () => {
	new Promise((resolve) => {
		device.publish(
			publishingTopic,
			// dataToPush,
			JSON.stringify(dataToPush),
			pubOpts,
			(err, data) => {
				if (err === null) {
					return resolve(true);
				}
				return resolve(false);
			}
		);
	});
	result.length = 0;
};

const read = async (time) => {
	cron.schedule(`${time} * * * * *`, async () => {
		//   console.log(deviceList);
		for (const eachDevice of deviceList) {
			try {
				// console.log(eachDevice);
				// console.log(eachDevice.ref);
				// console.log(eachDevice.address);
				if (eachDevice.ref === 1 || eachDevice.ref === 2) {
					// ref 1, 2 is Actuator

					let setPoint = 100;
					try {
						await client.setID(eachDevice.ref);

						await new Promise((resolve) => setTimeout(resolve, 10));
						energyParameters = await client.readHoldingRegisters(
							eachDevice.address,
							1
						);
						await new Promise((resolve) => setTimeout(resolve, 10));
						setPoint = energyParameters.data[0] / 100;
					} catch (e) {
						console.log(e);
					}
					// const setPoint = 100;
					let output = {
						ref: eachDevice.ref,
						setPoint: setPoint,
					};
					result.push(output);
				}
			} catch (e) {
				console.log(e);
			}
		}
		for (const eachObject of result) {
			eachObject.deviceId = deviceId;
			eachObject.lastUpdate = moment().unix();
		}
		console.log(result);
		dataToPush.data = result;
		published();
	});
};

main();
